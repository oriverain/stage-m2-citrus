# PFE

## Nom
Analyse d’images 3D de peaux d’agrumes

## Developpeur
RIVERAIN Olivier

## Description
Stage Master 2 Informatique pour l'Image et le Son

## Installation

**3D Slicer**

**libglu1-mesa libpulse-mainloop-glib0 libnss3 libasound2 qt5dxcb-plugin libsm6** (voir https://slicer.readthedocs.io/en/latest/user_guide/getting_started.html#installing-3d-slicer)

### programme python créés
displayProfile.py à mettre dans le dossier de build après avoir tout compilé

### Packages installés

**python3-numpy python3-opencv python3-matplotlib**

**cmake**

**qtbase5-dev (5.15.8)**

**qt5-image-formats-plugins** (ne supporte pas jp2)

**qt6-base-dev (6.4.2)**

**qt6-image-formats-plugins** (ne supporte pas jp2)

**libopencv-dev** (supporte  jp2)

**libmagick++-6.q16-dev** (supporte jp2)

**libcppunit-dev**

**itk libinsighttoolkit5-dev**

**vtk libvtk9-dev libvtk9-qt-dev**

**libqglviewer-dev-qt5** (au cas où pour l'instant sert pour dgtal)

**libqt5charts5-dev**

**libboost-all-dev**

**DGtal DGtal-1.3** compilé directement depuis les sources

Quand on fait le ccmake modifier

ITK_DIR ON

WITH_QGLVIEWER ON

make

make install

## Compilation

récupérer l'archive

la désarchiver

mkdir build

cd build

cmake ../citrus-skins-main/

make

./citrus-skins

./testCitrus

## Captures d'écran
### Fenêtre principale
![Fenêtre principale](/captures/fenetre_ppale_01.png  "Fenêtre principale")
![Menu File](/captures/menu_file.png  "Menu File")
![Menu Tools](/captures/menu_tools.png  "Menu Tools")
### View2D
![View2D explorateur de fichier](/captures/view_2D_filedialog.png  "View2D explorateur de fichier")
![View2D](/captures/view2D_widget.png  "View2D")
### Convert Format
![View2D convert explorateur de fichier](/captures/convert_opendirectory.png  "View2D convert explorateur de fichier")
![View2D convert](/captures/convert_widget_01.png  "View2D convert")
### View3D
![View3D explorateur de fichier](/captures/file_directory_view3D.png  "View3D explorateur de fichier")
![View3D](/captures/pamplemousse_view3D.png  "View3D")
### Profil
![View2D profil](/captures/profile.png  "View2D profil")
![View2D affichage profil](/captures/profile_sphere_p4.png  "View2D affichage profil")

## Vidéo
Une vidéo de présentation video-pfe-citrus.mp4 se trouve dans le dossier captures.


![vidéo](/captures/video-pfe-citrus.mp4  "Vidéo")

