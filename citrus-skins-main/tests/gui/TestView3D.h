#ifndef TESTVIEW3D_H
#define TESTVIEW3D_H
#include <cppunit/extensions/HelperMacros.h>
#include  "../../src/gui/View3D.h"

// Define class TestView3D
class TestView3D : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(TestView3D);
    CPPUNIT_TEST(testLoadFile);
    CPPUNIT_TEST_SUITE_END();

public:
    void testLoadFile();

    void setUp();
    void tearDown();

    View3D *view;
    std::string format;
};
#endif
