#include "TestView3D.h"
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>
#include <QApplication>
#include <QDebug>

CPPUNIT_TEST_SUITE_REGISTRATION(TestView3D);

// Define setUp for the tests
void TestView3D::setUp(){
    int argc=0;
    char *argv[] = {nullptr} ;
    QApplication app(argc, argv);
    view = new View3D();
}

// Define tearDown for the tests
void TestView3D::tearDown(){
    delete view;
}

// Test if the format file is processed the right way
void TestView3D::testLoadFile(){
    format = "png";
    CPPUNIT_ASSERT_EQUAL(1, view->loadFile(format));
    format = "jpeg";
    CPPUNIT_ASSERT_EQUAL(1, view->loadFile(format));
    format = "mp3";
    CPPUNIT_ASSERT_EQUAL(1, view->loadFile(format));
    format = "dcm";
    CPPUNIT_ASSERT_EQUAL(0, view->loadFile(format));
    format = "nrrd";
    CPPUNIT_ASSERT_EQUAL(0, view->loadFile(format));
    format = "tif";
    CPPUNIT_ASSERT_EQUAL(0, view->loadFile(format));
    format = "tiff";
    CPPUNIT_ASSERT_EQUAL(0, view->loadFile(format));
    std::cout << "testLoadFile OK" << std::endl;
}
