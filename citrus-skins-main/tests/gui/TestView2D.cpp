#include "TestView2D.h"
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>
#include <QApplication>
#include <QDebug>

CPPUNIT_TEST_SUITE_REGISTRATION(TestView2D);

// Define setUp for the tests
void TestView2D::setUp(){
        int argc = 0;
        char *argv[] = {nullptr};
        QApplication app(argc, argv);
        view = new View2D();
        QString file1 = "file1.tiff";
        QString file2 = "file2.tiff";
        QString file3 = "file3.tiff";
        view->appendToList(file1);
        view->appendToList(file2);
        view->appendToList(file3);
}

// Define tearDown for the tests
void TestView2D::tearDown(){
        delete view;
}

// Define the test for the function testGetIndex
void TestView2D::testGetIndex(){
        CPPUNIT_ASSERT_EQUAL(-1, view->getIndex("nonexistent.tiff"));
        CPPUNIT_ASSERT_EQUAL(0, view->getIndex("file1.tiff"));
        CPPUNIT_ASSERT_EQUAL(1, view->getIndex("file2.tiff"));
        CPPUNIT_ASSERT_EQUAL(2, view->getIndex("file3.tiff"));
        std::cout << "testGetIndex OK" << std::endl;
}

// Define the test for the function testMove
void TestView2D::testMove(){
        CPPUNIT_ASSERT_EQUAL(-1, view->move(-2));
        CPPUNIT_ASSERT_EQUAL(-1, view->move(3));
        std::cout << "testMove OK" << std::endl;
}

// Define the test for the function ConvertFilesToFormat
void TestView2D::testConvertFilesToFormat() {
        int begin = 0;
        int begin_n = -10;
        int begin_p = 10;
        int end = 2;
        int end_n = -10;
        int end_p = 10;

        QString directoryDest = "/home/olivier";
        QString format = "jpg";
        CPPUNIT_ASSERT_EQUAL(-1,view->convertFilesToFormat(begin_n,end, format, directoryDest));
        CPPUNIT_ASSERT_EQUAL(-1,view->convertFilesToFormat(begin,end_n, format, directoryDest));
        CPPUNIT_ASSERT_EQUAL(-1,view->convertFilesToFormat(begin_p,end, format, directoryDest));
        CPPUNIT_ASSERT_EQUAL(-1,view->convertFilesToFormat(begin,end_p, format, directoryDest));
        CPPUNIT_ASSERT_EQUAL(-1,view->convertFilesToFormat(begin_p,end_p, format, directoryDest));
        std::cout << "testConvertFilesToFormat OK" << std::endl;
}