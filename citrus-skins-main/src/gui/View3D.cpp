#include <vtkImageData.h>
#include <vtkNamedColors.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkNrrdReader.h>
#include <vtkTIFFReader.h>
#include <vtkDICOMImageReader.h>
#include <vtkImageViewer2.h>
#include <vtkVolumeProperty.h>
#include <vtkColorTransferFunction.h>
#include <vtkPiecewiseFunction.h>
#include <vtkVolume.h>
#include <vtkOpenGLGPUVolumeRayCastMapper.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>

#include <QDebug>
#include <iostream>
#include <QString>
#include <QFileDialog>
#include <QFileInfo>

#include "View3D.h"

// Define the constructor of the class View3D
View3D::View3D(QWidget *parent)
    :QMainWindow(parent)
{
}

// Get file data and return a reader according to the format
int View3D::loadFile(std::string format){
    // Create the correct reader depending on file format
    if(format == "nrrd"){
        vtkSmartPointer<vtkNrrdReader> nrrdReader = vtkSmartPointer<vtkNrrdReader>::New();
        reader = nrrdReader;
    }else if(format == "tiff" || format == "tif"){
        vtkSmartPointer<vtkTIFFReader> tiffReader = vtkSmartPointer<vtkTIFFReader>::New();
        reader = tiffReader;
    }else if(format == ".dcm"){
        vtkSmartPointer<vtkDICOMImageReader> dicomReader = vtkSmartPointer<vtkDICOMImageReader>::New();
        reader = dicomReader;
    }else{
        return 1;
    }
    return 0;
}

// Display information on the render window
void View3D::displayInfo(double* range, int* dimensions){
    if(range != nullptr && dimensions != nullptr){
        vtkSmartPointer<vtkTextActor> textActor = vtkSmartPointer<vtkTextActor>::New();

        std::string strRange = "Scalar Range: " + std::to_string(int(range[0])) + " - " + std::to_string(int(range[1]));
        std::string strDimensions = "Dimensions: (" +  std::to_string(dimensions[0]) + "," + std::to_string(dimensions[1]) + "," + std::to_string(dimensions[2]) + ")";

        textActor->SetInput((strDimensions + "\n" + strRange).c_str());
        textActor->SetPosition2(10, 40);
        textActor->GetTextProperty()->SetFontSize(20);
        textActor->GetTextProperty()->SetFontFamilyToArial();
        textActor->GetTextProperty()->SetColor(0,0,0);
        renderer->AddActor2D(textActor);
    }
}

// Load and display the volume in 3D
int View3D::createView3D(){
    QString filename = QFileDialog::getOpenFileName(this,  tr("Open Image"), "..", tr("Image Files (*.nrrd *.tiff *.tif *.dcm)"));

    if(filename == nullptr){
        return 1;
    }

    QFileInfo fileInfo(filename);
    std::string str = fileInfo.fileName().toStdString();
    std::string format = (str).substr(str.find_last_of(".") + 1);

    if(loadFile(format) == 1){
        std::cerr << "Choose and existing file or choose a file whose format is supported (.nnrd, .tif, .tiff & .dcm)" << std::endl;
        return 1;
    }

    reader->SetFileName(filename.toLocal8Bit().data());
    reader->Update();

    // Get image data to display its range values
    vtkImageData* imageData = reader->GetOutput();
    int dimensions[3];
    imageData->GetDimensions(dimensions);
    std::cout << "Dimensions: (" << dimensions[0] << "," << dimensions[1] << "," << dimensions[2] << ")" << std::endl;

    double range[2];
    imageData->GetScalarRange(range);
    std::cout << "Scalar Range: " << range[0] << " - " << range[1] << std::endl;

    // Create transfer mapping scalar value to opacity
    vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
    opacityTransferFunction->AddPoint(32000, 0.0); // Background is completely transparent
    opacityTransferFunction->AddPoint(65536, 1); // Object voxels are displayed normally

    // Create transfer mapping scalar value to color
    vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction>::New();
    colorTransferFunction->AddRGBPoint(0.0, 0.0, 0.0, 0.0); // Background is black
    colorTransferFunction->AddRGBPoint(1.0, 0.0, 0.5, 0.5); // Object voxels are blue

    // The property describes how the data will look
    vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();
    volumeProperty->SetColor(colorTransferFunction);
    volumeProperty->SetScalarOpacity(opacityTransferFunction);
    volumeProperty->ShadeOn();
    volumeProperty->SetInterpolationTypeToLinear();

    // Mapper function to render the data
    vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper> volumeMapper = vtkSmartPointer<vtkOpenGLGPUVolumeRayCastMapper>::New();
    volumeMapper->SetInputConnection(reader->GetOutputPort());

    // The volume holds the mapper and the property and can be used to position/orient the volume
    vtkSmartPointer<vtkVolume> volume = vtkSmartPointer<vtkVolume>::New();
    volume->SetMapper(volumeMapper);
    volume->SetProperty(volumeProperty);

    // Create the renderer, render window, and interactor
    renderer = vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);
    renderWindow->SetWindowName((fileInfo.fileName().toStdString()).c_str());
    renderWindow->SetSize(800,800);
    vtkSmartPointer<vtkRenderWindowInteractor> interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    interactor->SetRenderWindow(renderWindow);

    displayInfo(range,dimensions);

    // Add the volume to the renderer
    renderer->AddVolume(volume);
    renderer->SetBackground(1, 1, 1); // Set background color to white

    // Render and start the interactor
    renderWindow->Render();
    interactor->Start();

    return 0;
}