#include <iostream>
#include <string>
#include <QFileDialog>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QImage>
#include <QImageReader>
#include <QPushButton>
#include <QFileInfo>
#include <QDir>
#include <QStringList>
#include <QSlider>
#include <QDebug>
#include <QTextEdit>
#include <QComboBox>
#include <QSize>
#include <QProgressBar>

#include "View2D.h"
#include "../toolbox/ToolImage.h"
#include "../toolbox/ToolImage3D.h"

#include <Magick++.h>
using namespace Magick;


// Define the constructor of the class View2D
View2D::View2D(QWidget *parent)
    :QMainWindow(parent)
{
}

// Call the functions to create a View2D
void View2D::createView2D(){
    setWindowTitle(tr("View 2D Window"));
    createView2DBegin();
    view2DNavigation();
    view2DOpen();
    createView2DEnd();
}

// Create the containers
void View2D::createView2DBegin(){
    mainWidget = new QWidget;
    vboxleft = new QVBoxLayout(this);
    vboxright = new QVBoxLayout(this);
    hbox = new QHBoxLayout(this);
    hboxSlider = new QHBoxLayout(this);
    hboxSlider2 = new QHBoxLayout(this);
    hboxPrevisual = new QHBoxLayout(this);
    hboxProgressBar = new QHBoxLayout(this);

    vboxleft->setSpacing(2);
    vboxright->setSpacing(3);
}

// Add the containers in the main widget
void View2D::createView2DEnd(){
    hbox->addLayout(vboxleft);
    hbox->addLayout(vboxright);
    mainWidget->setLayout(hbox);
    setCentralWidget(mainWidget);
}

// Load and display the image in 2D
void View2D::view2DDisplayImage(){
    filename = QFileDialog::getOpenFileName(this,  tr("Open Image"), "..", tr("Image Files (*.jp2 *.png *.jpg *.bmp *.tiff)"));    
    QFileInfo fileInfo(filename);
    directoryname = fileInfo.path();
    filename_short = fileInfo.fileName();
    QString extension = fileInfo.suffix();    
    QDir directory(directoryname);
    directory.setFilter(QDir::Files);
    directory.setSorting(QDir::Name);
    fileList = directory.entryList();    
    qDebug() << "extension: " << extension ;
    if(extension.compare("jp2") == 0) {
        qDebug() << "cas jpeg2000: " ;
        qDebug() << "filename: " << filename;
        ToolImage tool;
        
        Image image(filename.toStdString());

        // Obtenir la taille de l'image
        unsigned int width = image.columns();
        unsigned int height = image.rows();
        std::string sizeTmp;
        if(width >= height) sizeTmp = std::to_string( width) ; else sizeTmp = std::to_string(height);

        // Afficher la taille de l'image
        std::cout << "Largeur de l'image : " << width << " pixels" << std::endl;
        std::cout << "Hauteur de l'image : " << height << " pixels" << std::endl;
        std::cout << "sizeTmp : " << sizeTmp << std::endl;
        tool.convert(filename.toStdString(), "./tmp.jpg", std::to_string(thumbnailWidth));
        //Image imageTmp("./tmp.jpg");
        QImageReader qImageTmp("./tmp.jpg");
        qDebug() << "Image Format: " << qImageTmp.format() ;
        sizeImage = qImageTmp.size();
        qDebug() << "Image Size: " << sizeImage;
        imageTmp = qImageTmp.read();
        qDebug() << "Is Image Null: " << imageTmp.isNull( );
        thumbnailLabel = new QLabel(this);
        thumbnailLabel->setPixmap(QPixmap::fromImage(imageTmp.scaled(thumbnailWidth, thumbnailHeight, Qt::KeepAspectRatio)));

    } else {  
    QImageReader qImage(filename);
    qDebug() << "Image Format: " << qImage.format() ;
    sizeImage = qImage.size();
    qDebug() << "Image Size: " << sizeImage;
    image = qImage.read();
    qDebug() << "Is Image Null: " << image.isNull( );
    thumbnailLabel = new QLabel(this);
    thumbnailLabel->setPixmap(QPixmap::fromImage(image.scaled(thumbnailWidth, thumbnailHeight, Qt::KeepAspectRatio)));
    }
       
    
    labelName = new QLabel(filename_short, this);
}

// Create the widget to navigate into the list of images
void View2D::view2DNavigation(){
    view2DDisplayImage();

    QPushButton *buttonPrev = new QPushButton(tr("Prev"));
    buttonPrev->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    slider = new QSlider(Qt::Horizontal, this);
    slider->setRange(0, fileList.size()-1);
    slider->setValue(getIndex(filename_short));   

    QPushButton *buttonNext = new QPushButton(tr("Next"));
    buttonNext->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QPushButton *buttonSlider = new QPushButton(tr("Go"));
    buttonSlider->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    labelSliderMin = new QLabel("0", this);
    labelSliderMax = new QLabel(QString::number(fileList.size()-1), this);  

    sliderValue = new QTextEdit(QString::number(getIndex(filename_short)));    
    sliderValue->setMaximumHeight(sliderValue->fontMetrics().height()+10);
    sliderValue->setMaximumWidth(70);

    previsualSize = new QTextEdit(QString::number(thumbnailWidth));
    previsualSize->setMaximumHeight(previsualSize->fontMetrics().height()+10);
    previsualSize->setMaximumWidth(70);
    QPushButton *buttonPreview = new QPushButton(tr("Preview"));
    buttonPreview->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    connect(buttonPrev, &QPushButton::clicked, this, &View2D::prev);
    connect(buttonNext , &QPushButton::clicked, this, &View2D::next);    
    connect(slider, &QSlider::valueChanged, this, &View2D::updateSliderValue);
    connect(slider, &QSlider::sliderReleased, this, &View2D::releaseSlider);
    connect(buttonSlider, &QPushButton::clicked, this, &View2D::go);
    connect(buttonPreview, &QPushButton::clicked, this, &View2D::preVisual);

    hboxSlider->addWidget(buttonPrev);
    hboxSlider->addWidget(labelSliderMin);
    hboxSlider->addWidget(slider);
    hboxSlider->addWidget(labelSliderMax);
    hboxSlider->addWidget(buttonNext);
    hboxPrevisual->addWidget(previsualSize);
    hboxPrevisual->addWidget(buttonPreview);
    vboxleft->addWidget(thumbnailLabel);
    vboxleft->addLayout(hboxPrevisual);
    vboxleft->addLayout(hboxSlider);
    hboxSlider2->addWidget(sliderValue);
    hboxSlider2->addWidget(buttonSlider);
    vboxleft->addLayout(hboxSlider2);
    vboxleft->addWidget(labelName);   
}

// Create the widget to open an image
void View2D::view2DOpen(){
    QPushButton *buttonOpen = new QPushButton(tr("Open"));
    buttonOpen->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    connect(buttonOpen, &QPushButton::clicked, this, &View2D::open);
    vboxleft->addWidget(buttonOpen);
}

// Convert an image in an other format
void View2D::view2DConvert(){
    beginFile = new QTextEdit(QString::number(0));
    beginFile->setMaximumHeight(previsualSize->fontMetrics().height()+10);
    beginFile->setMaximumWidth(70);
    endFile = new QTextEdit(QString::number(fileList.size()-1));
    endFile->setMaximumHeight(previsualSize->fontMetrics().height()+10);
    endFile->setMaximumWidth(70);
    comboBox = new QComboBox(this);
    comboBox->addItem("tiff");
    comboBox->addItem("jpg");
    comboBox->addItem("png");
    comboBox->addItem("nrrd");
    comboBox->setCurrentIndex(0);
    selectedFormat = comboBox->currentText();    
    sizeFile = new QTextEdit(QString::number(sizeImage.width()));
    sizeFile->setMaximumHeight(previsualSize->fontMetrics().height()+10);
    sizeFile->setMaximumWidth(70);


    QPushButton *buttonConvert = new QPushButton(tr("Convert"));
    buttonConvert->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QPushButton *buttonDirectory = new QPushButton(tr("Directory"));

    connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &View2D::updateComboBox);
    connect(buttonConvert, &QPushButton::clicked, this, &View2D::convertFiles);
    connect(buttonDirectory, &QPushButton::clicked, this, &View2D::convertDirectory);

    hboxConvert = new QHBoxLayout(this);
    hboxConvert->addWidget(beginFile);
    hboxConvert->addWidget(endFile);
    hboxConvert->addWidget(comboBox);
    hboxConvert->addWidget(sizeFile);
    hboxConvert->addWidget(buttonDirectory);
    hboxConvert->addWidget(buttonConvert);
    vboxleft->addLayout(hboxConvert);
}

// Create a progress bar
void View2D::view2DProgressBar(){    
    progressBar = new QProgressBar(this);   
    progressBar->setOrientation(Qt::Horizontal);
    hboxProgressBar->addWidget(progressBar);
    vboxleft->addLayout(hboxProgressBar);    
}

// Go to the previous image
void View2D::prev(){
    int index = getIndex(filename_short);
    int new_index = 0;
    if(index>0){
        new_index = index-1;
        move(new_index);
    }
}

// Retrieve begin and end to convert a range of images in an other format
void View2D::convertFiles(){
    int begin = (beginFile->toPlainText()).toInt();
    int end = (endFile->toPlainText()).toInt();
    convertFilesToFormat(begin, end, selectedFormat, directoryDest);
}

// Define the dest directory where to convert images
void View2D::convertDirectory(){
    directoryDest = QFileDialog::getExistingDirectory(this,  tr("Open Directory"), "..", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    directoryDest.append("/");    
}

// Convert a range of images in an other format
int View2D::convertFilesToFormat(int begin, int end, QString format, QString directoryDest){
    if(begin < 0 || end > fileList.size()-1) return -1;
    if(begin > end) return -1;
    ToolImage tool;
    ToolImage3D tool3D;
    int index = begin-1;
    progressBar->setRange(begin, end);
    if(selectedFormat.toStdString() != "nrrd"){
    for(int i=begin; i<end+1; i++) {
        QString fileNameToConvert = directoryname;
        fileNameToConvert.append("/");
        fileNameToConvert.append(fileList.at(i));        
        QFileInfo fileInfo(fileNameToConvert);       

        tool.convert(fileNameToConvert.toStdString(), directoryDest.toStdString() + (fileInfo.completeBaseName()).toStdString()+"." + format.toStdString(), (sizeFile->toPlainText()).toStdString());
        index++;
        progressBar->setValue(index);
        }
    } else {
        qDebug() << "avant convert3D";
        std::string outputfilename = (filename.toStdString()).substr(0, (filename.toStdString()).find_last_of(".")) + ".nrrd" ;
        std::cout << "filename = " << filename.toStdString() << std::endl;;
        std::cout << "outputfilename = " << outputfilename << std::endl;
        qDebug() << "directoryName = " << directoryname;
        qDebug() << "directoryDest = " << directoryDest;
        tool3D.convert3D(begin, end, directoryname.toStdString(), directoryDest.toStdString(), outputfilename, (sizeFile->toPlainText()).toStdString(), fileList);
        
        qDebug() << "apres convert3D";
    }

    return 0;
}

// Go to the image defined by new_index
int View2D::move(int new_index){    
    if(new_index < 0 || new_index >= fileList.size()) return -1;
    filename_short = fileList.at(new_index);
    labelName->setText(filename_short);
    filename = directoryname;
    filename += "/";
    filename += filename_short;
    QFileInfo fileInfo(filename);
    QString extension = fileInfo.suffix();
    if(extension.compare("jp2") == 0) {
        qDebug() << "cas jpeg2000: " ;
        qDebug() << "filename: " << filename;
        ToolImage tool;
        
        Image image(filename.toStdString());

        // Obtenir la taille de l'image
        unsigned int width = image.columns();
        unsigned int height = image.rows();
        std::string sizeTmp;
        if(width >= height) sizeTmp = std::to_string( width) ; else sizeTmp = std::to_string(height);

        // Afficher la taille de l'image
        std::cout << "Largeur de l'image : " << width << " pixels" << std::endl;
        std::cout << "Hauteur de l'image : " << height << " pixels" << std::endl;
        std::cout << "sizeTmp : " << sizeTmp << std::endl;
        tool.convert(filename.toStdString(), "./tmp.jpg", std::to_string(thumbnailWidth));        
        QImageReader qImageTmp("./tmp.jpg");
        qDebug() << "Image Format: " << qImageTmp.format() ;
        sizeImage = qImageTmp.size();
        qDebug() << "Image Size: " << sizeImage;
        imageTmp = qImageTmp.read();
        qDebug() << "Is Image Null: " << imageTmp.isNull( );        
        thumbnailLabel->setPixmap(QPixmap::fromImage(imageTmp.scaled(thumbnailWidth, thumbnailHeight, Qt::KeepAspectRatio)));
    } else {
    QImageReader qImage(filename);
    image = qImage.read();    
    thumbnailLabel->setPixmap(QPixmap::fromImage(image.scaled(thumbnailWidth, thumbnailHeight, Qt::KeepAspectRatio)));
}
    slider->setValue(new_index);

    filename_short = fileList.at(new_index);
    labelName->setText(filename_short);
    return new_index;
}

// Go to the next image
void View2D::next(){
    int index = getIndex(filename_short);
    int new_index = 0;    
    if(index<fileList.size()-1){
        new_index = index+1;
        move(new_index);
    }
}

//Go to the image when the slider is released 
void View2D::releaseSlider() {
    go();
}

// Open the original image
void View2D::open(){
   ToolImage tool;
   tool.display(filename.toStdString());
}

// Go to the image defined by new_index
void View2D::go(){    
    int newIndex = (sliderValue->toPlainText()).toInt();
    move(newIndex);
}


// Return the index of an image in the list of images
int View2D::getIndex(QString filename){
    for(int i=0; i < fileList.size(); i++){
        if(QString::compare(filename, fileList.at(i)) == 0) return i;
    }
    return -1;
}

// Update the value of the slider
void View2D::updateSliderValue(){
    sliderValue->setPlainText(QString::number(slider->value()));
}

// Retrieve the width and height and do the previsualization
void View2D::preVisual(){
    thumbnailWidth = (previsualSize->toPlainText()).toInt();
    thumbnailHeight = thumbnailWidth;
    int newIndex = (sliderValue->toPlainText()).toInt();
    move(newIndex);
}

// Append the name of an image to the list of images
void View2D::appendToList(QString name){
    fileList.append(name);
}

// Create the widget convert to
void View2D::createConvertTo(){
    setWindowTitle(tr("Convert to Window"));
    createView2DBegin();
    view2DNavigation();
    view2DConvert();
    view2DProgressBar();
    createView2DEnd();
}

// Update the selected format in the comboBox
void View2D::updateComboBox(){
    selectedFormat = comboBox->currentText();
    qDebug() << "Selected Format: " << selectedFormat;
}


