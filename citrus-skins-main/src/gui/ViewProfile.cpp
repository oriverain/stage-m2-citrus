#include "ViewProfile.h"
#include <iostream>
#include <string>
#include "../toolbox/ToolGeom.h"

#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QtCharts>
#include <QCheckBox>
#include <QTextEdit>
#include <QPushButton>

using namespace DGtal;

// Define the constructor of the class ViewProfile
ViewProfile::ViewProfile(QWidget *parent)
    :QMainWindow(parent)
{
}

// Create the profile
void ViewProfile::createProfile(){
    setWindowTitle(tr("View Profile Window"));
    createProfileBegin();
    std::string extension = (filename.toStdString()).substr((filename.toStdString()).find_last_of(".") + 1);
    if(extension.compare("txt") ==0){
        displayProfile(filename);
    }else{
        ViewProfileContainer();
    }
    createProfileEnd();
}

// Create the containers
void ViewProfile::createProfileBegin(){
    mainWidget = new QWidget;
    vboxleft = new QVBoxLayout(this);
    vboxright = new QVBoxLayout(this);
    hbox = new QHBoxLayout(this);
    hboxContainer = new QHBoxLayout(this);
    filename = QFileDialog::getOpenFileName(this,  tr("Open File"), "..", tr("Files (*.txt  *.nrrd)"));
    QFileInfo fileInfo(filename);
}

// Add the containers in the main widget
void ViewProfile::createProfileEnd(){
    hbox->addLayout(vboxleft);
    hbox->addLayout(vboxright);
    mainWidget->setLayout(hbox);
    setCentralWidget(mainWidget);
}

// Create the container with the checked case and point
void ViewProfile::ViewProfileContainer(){
    checkBoxNormal = new QCheckBox(tr("Normal"), this);
    checkBoxNormal->setChecked(false);
    checkBoxMean = new QCheckBox(tr("Mean"), this);
    checkBoxMean->setChecked(false);
    pxEdit = new QTextEdit(QString::number(0));
    pxEdit->setMaximumHeight(pxEdit->fontMetrics().height()+10);
    pxEdit->setMaximumWidth(70);
    pyEdit = new QTextEdit(QString::number(0));
    pyEdit->setMaximumHeight(pyEdit->fontMetrics().height()+10);
    pyEdit->setMaximumWidth(70);
    pzEdit = new QTextEdit(QString::number(0));
    pzEdit->setMaximumHeight(pzEdit->fontMetrics().height()+10);
    pzEdit->setMaximumWidth(70);
    QPushButton *buttonProfile = new QPushButton(tr("Profile"));
    buttonProfile->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);


    connect(checkBoxNormal, &QCheckBox::stateChanged, this, &ViewProfile::majNormal);
    connect(checkBoxMean, &QCheckBox::stateChanged, this, &ViewProfile::majMean);
    connect(buttonProfile , &QPushButton::clicked, this, &ViewProfile::goProfile);

    hboxContainer->addWidget(checkBoxNormal);
    hboxContainer->addWidget(checkBoxMean);
    hboxContainer->addWidget(pxEdit);
    hboxContainer->addWidget(pyEdit);
    hboxContainer->addWidget(pzEdit);
    hboxContainer->addWidget(buttonProfile);
    vboxleft->addLayout(hboxContainer);
}

// Display the profile
int ViewProfile::displayProfile(QString inputfilename){
    QFile file(inputfilename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "Can't open " << inputfilename;
        return -1;
    }

    QVector<QPoint> points;
    QTextStream in(&file);
    int cpt = 0;
    QString origin = "";
    QString destination = "";
    while(!in.atEnd()){
        QString line = in.readLine();
        QStringList tokens = line.split(" ");
        if(tokens.size() == 4){
            if(cpt==0){
                origin = "(" + tokens[0] + ", " + tokens[1] + ", " + tokens[2] + ")";
            }
            destination = "(" + tokens[0] + ", " + tokens[1] + ", " + tokens[2] + ")";
            int x = cpt;
            int y = tokens[3].toInt();
            points.append(QPoint(x, y));
            cpt++;
        }
    }

    file.close();

    QChart *chart = new QChart();
    QLineSeries *series = new QLineSeries();
    for(const QPoint &point : points){
        series->append(point.x(), point.y());
    }
    chart->addSeries(series);
    chart->setTitle("Profile: " + origin + " -> " + destination);
    chart->createDefaultAxes();
    chart->axes(Qt::Horizontal).first()->setTitleText(tr("List of the points"));
    chart->axes(Qt::Vertical).first()->setTitleText(tr("Level of the pixel"));

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->resize(800, 600);
    chartView->show();

    return 0;

}

// Update bool normal
void ViewProfile::majNormal(){
    normal = checkBoxNormal->isChecked();
}

// Update bool mean
void ViewProfile::majMean(){
    mean = checkBoxMean->isChecked();
}

// Go to the profile
void ViewProfile::goProfile(){
    computeProfile();
}

// Go to the profile
int ViewProfile::computeProfile(){
    int px = (pxEdit->toPlainText()).toInt();
    int py = (pyEdit->toPlainText()).toInt();
    int pz = (pzEdit->toPlainText()).toInt();
    DGtal::Z3i::Point p(px,py,pz);
    ToolGeom tool;
    trace.beginBlock("Compute Profile.");
    int res = tool.computeProfile(filename.toStdString(), normal, mean, p);
    trace.endBlock();
    if(res!=0) return -1;
    QString inputFilename = filename;
    inputFilename.append("_N");
    if(normal) inputFilename.append("true"); else inputFilename.append("false");
    inputFilename.append("_M");
    if(mean) inputFilename.append("true"); else inputFilename.append("false");
    inputFilename.append("_profile.txt");
    displayProfile(inputFilename);
    return 0;
}