#ifndef VIEWPROFILE_H
#define VIEWPROFILE_H

#include <QMainWindow>
#include <QtWidgets>

// Define class ViewProfile
class ViewProfile : public QMainWindow{
    Q_OBJECT

public:
    ViewProfile(QWidget *parent = nullptr);
    void createProfile();
    void createProfileBegin();
    void createProfileEnd();
    int displayProfile(QString f);
    void ViewProfileContainer();
    int computeProfile();
    
protected:

private slots:
    void majNormal();
    void majMean();
    void goProfile();

private:
    QWidget *mainWidget;
    QVBoxLayout *vboxleft;
    QVBoxLayout *vboxright;
    QHBoxLayout *hbox;
    QString filename;
    QString directoryname;
    QString filename_short;
    QHBoxLayout *hboxContainer;
    bool normal = false;
    QCheckBox *checkBoxNormal;
    QCheckBox *checkBoxMean;
    bool mean = false;
    QTextEdit * pxEdit;
    QTextEdit * pyEdit;
    QTextEdit * pzEdit;
};
#endif
