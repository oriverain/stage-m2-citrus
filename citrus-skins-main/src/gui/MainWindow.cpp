#include <QtWidgets>
#include <QToolBar>
#include <QIcon>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QTextEdit>
#include <QTextStream>
#include <iostream>
#include "MainWindow.h"
#include "View2D.h"
#include "View3D.h"
#include "ViewProfile.h"

// Create the main window with the menus in QT
MainWindow::MainWindow(QWidget *parent): QMainWindow(parent){
    QPixmap quitpix("images/quit.png");    
    auto *quit = new QAction(tr("&Quit"), this);
    quit->setShortcut(tr("CTRL+Q"));
    auto *view2D = new QAction(tr("&View2D"), this);
    view2D->setShortcut(tr("CTRL+&"));
    auto *view3D = new QAction(tr("&View3D"), this);
    view3D->setShortcut(tr("CTRL+é"));
    auto *convertFormat = new QAction(tr("&Convert Format"), this);
    convertFormat->setShortcut(tr("CTRL+\""));
    auto *viewProfile = new QAction(tr("&Profile"), this);
    viewProfile->setShortcut(tr("CTRL+p"));

    QMenu *file = menuBar()->addMenu("&File");
    file->addAction(quit);

    QMenu *tools = menuBar()->addMenu("&Tools");
    tools->addAction(view2D);
    tools->addAction(view3D);
    tools->addAction(convertFormat);
    tools->addAction(viewProfile);

    qApp->setAttribute(Qt::AA_DontShowIconsInMenus, false);

    connect(quit, &QAction::triggered, qApp, &QApplication::quit);
    connect(view2D, &QAction::triggered, this, &MainWindow::view2D);
    connect(view3D, &QAction::triggered, this, &MainWindow::view3D);
    connect(convertFormat, &QAction::triggered, this, &MainWindow::convertFormatTo);
    connect(viewProfile, &QAction::triggered, this, &MainWindow::createDisplayProfile);

    QToolBar *toolbar = addToolBar("main toolbar");
    QAction *quit2 = toolbar->addAction(QIcon(quitpix), "Quit Application");
    connect(quit2, &QAction::triggered, qApp, &QApplication::quit);

    statusBar()->showMessage("Ready");
}

// Call the window View2D
void MainWindow::view2D(){
  QTextStream out(stdout);  

  View2D *view2DWindow = new View2D(this);
  view2DWindow->createView2D();
  view2DWindow->show();
}

// Call the window View3D
void MainWindow::view3D(){
  QTextStream out(stdout);  
  View3D *view3DWindow = new View3D(this);
  view3DWindow->createView3D();
  view3DWindow->show();
}

// Call the window convertFormatTo
void MainWindow::convertFormatTo(){
  QTextStream out(stdout);  
  View2D *convertFormatWindow = new View2D(this);
  convertFormatWindow ->createConvertTo();
  convertFormatWindow->show();
}

// Call the window create and display Profile
void MainWindow::createDisplayProfile(){
  QTextStream out(stdout);  
  ViewProfile *createProfileWindow = new ViewProfile(this);
  createProfileWindow->createProfile();
  createProfileWindow->show();
}
