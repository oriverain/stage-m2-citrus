#ifndef VIEW3D_H
#define VIEW3D_H

#include <QMainWindow>
#include <QtWidgets>

#include <vtkSmartPointer.h>
#include <vtkImageReader2.h>
#include <vtkRenderer.h>

// Define class View3D
class View3D : public QMainWindow {
    Q_OBJECT

public:
    View3D(QWidget *parent = nullptr);
    int createView3D();
    int loadFile(std::string format);
    void displayInfo(double* range, int* dimensions);

protected:

private:
    vtkSmartPointer<vtkImageReader2> reader;
    vtkSmartPointer<vtkRenderer> renderer;
    QFileInfo fileInfo;

};
#endif