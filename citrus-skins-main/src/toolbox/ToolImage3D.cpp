#include <iostream>

#include "ToolImage3D.h"

#include "itkImage.h"
#include "itkImageSeriesReader.h"
#include "itkImageFileWriter.h"
#include "itkNumericSeriesFileNames.h"

#include <QStringList>

using namespace itk;
using namespace std;


// Convert a list of 2D images to a 3D image
int ToolImage3D::convert3D(int begin, int end, std::string inputDirectory, std::string outputDirectory, std::string outputFilename,  std::string size, QStringList fileList) {
  std::cout << "ToolImage3D::convert3D debut" << std::endl;
  std::string extension = (fileList.at(begin).toStdString()).substr((fileList.at(begin).toStdString()).find_last_of(".") + 1);
  std::string filename = (fileList.at(begin).toStdString()).substr(0, (fileList.at(begin).toStdString()).find_last_of(".")-5);
  std::cout << "extension = " << extension << std::endl;
  std::cout << "filename = " << filename << std::endl;
  std::cout << "inputDirectory = " << inputDirectory << std::endl;
  std::cout << "seriesname = " << inputDirectory + "/" + filename + "%05d" + "." + extension << std::endl;

  using PixelType = unsigned int;
  constexpr unsigned int Dimension = 3;

  using ImageType = itk::Image<PixelType, Dimension>;
  using ReaderType = itk::ImageSeriesReader<ImageType>;

  auto reader = ReaderType::New();

  using NameGeneratorType = itk::NumericSeriesFileNames;

  auto nameGenerator = NameGeneratorType::New();

  nameGenerator->SetSeriesFormat(inputDirectory + "/" + filename + "%05d" + "." + extension);
  nameGenerator->SetStartIndex(begin);
  nameGenerator->SetEndIndex(end);
  nameGenerator->SetIncrementIndex(1);
  std::vector<std::string> names = nameGenerator->GetFileNames();

  // List the files
  std::vector<std::string>::iterator nit;
  for (nit = names.begin(); nit != names.end(); ++nit)
  {
    std::cout << "File: " << (*nit).c_str() << std::endl;
  }

  clock_t start, cend;
  start = clock();
  reader->SetFileNames(names);
  outputFilename = outputDirectory + "/" + filename + "_" + std::to_string(begin) + "to" + std::to_string(end) + ".nrrd";
  std::cout << "ToolImage3D::convert3D outputFilename = " << outputFilename << std::endl;
  try
  {
    itk::WriteImage(reader->GetOutput(), outputFilename);
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }
  cend = clock();
  double time_taken = double(cend - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time itk::WriteImage : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;
  std::cout << "ToolImage3D::convert3D fin" << std::endl;
  return 0;
}
