#ifndef TOOLGEOM_H
#define TOOLGEOM_H

#include <string>
#include <DGtal/helpers/StdDefs.h>
#include "DGtal/images/ImageContainerBySTLVector.h"

typedef  DGtal::ImageContainerBySTLVector <DGtal::Z3i::Domain, unsigned char> Image3D;

// Define class ToolGeom
class ToolGeom{

public:
int computeProfile(std::string filename, bool normal, bool mean, DGtal::Z3i::Point p);
int computeNormals(Image3D image, double *normals,  DGtal::Z3i::Point p);

protected:

private:

};
#endif