#include <iostream>

#include "ToolImage.h"
#include <Magick++.h>
#include <opencv2/opencv.hpp>

using namespace Magick;

// Display an image with imageMagick
int ToolImage::display(const std::string filename){
    Image image;
    try{
        image.read(filename);
        image.display();
    }
    catch(Exception &error_ ){
        std:: cout << "Caught exception: " << error_.what() << std::endl;
        return -1;
    }
    return 0;
}

// Convert an image in an other format and in an other size  with ImageMagick
int ToolImage::convert(std::string inputfilename, std::string outputfilename,  std::string size){
    Image image;
    try{
        image.read(inputfilename);
        image.resize(size + "x" + size);
        image.write(outputfilename);
    }catch(Exception &error_ ){
        std:: cout << "Caught exception: " << error_.what() << std::endl;
        return -1;
    }
    return 0;
}

// Otsu segmentation for binarisation of the image (to only keep the citrus)
void ToolImage::segmentation_otsu(){
    cv::Mat image = cv::imread("../1.412um_pamplemousse2_pag_0424.tiff");

    if (image.empty()) {
        std::cerr << "Error: impossible to load the image" << std::endl;
        return;
    }

    // Convert the image in gray levels
    cv::Mat gray;
    cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

    // Apply Otsu segmentation
    cv::Mat segmentedImage;
    cv::threshold(gray, segmentedImage, 0, 128, cv::THRESH_BINARY + cv::THRESH_OTSU);

    cv::imshow("Segmented Image (Otsu)", segmentedImage);

    // Find the contours
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(segmentedImage, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    if (!contours.empty()) {
        // Find the smallest encompassing circle to obtain the radius
        cv::Point2f center;
        float radius;
        cv::minEnclosingCircle(contours[0], center, radius);

        // Create a circular mask with a smaller radius
        cv::Mat mask = cv::Mat::zeros(segmentedImage.size(), CV_8UC1);
        cv::circle(mask, center, static_cast<int>(radius * 0.7), cv::Scalar(255), -1);

        // Apply the mask to the original image to keep everything inside the circle
        cv::Mat croppedImage;
        image.copyTo(croppedImage, mask);

        // Display segmented image
        cv::imshow("Segmented Image (Otsu) - Cropped", croppedImage);
        cv::waitKey(0);
        cv::destroyAllWindows();
    } else {
        std::cerr << "Impossible to find contours to segment the image." << std::endl;
    }
}

