#include "ToolGeom.h"
#include <iostream>

#include <DGtal/io/readers/VolReader.h>
#include <DGtal/images/ImageSelector.h>
#include <DGtal/base/Common.h>
#include "DGtal/helpers/StdDefs.h"
#include "DGtal/base/BasicFunctors.h"
#include "DGtal/images/ImageContainerBySTLVector.h"
#include "DGtal/io/readers/ITKReader.h"

#include <bits/stdc++.h>

#include <DGtal/images/imagesSetsUtils/SetFromImage.h>
#include "DGtal/images/SimpleThresholdForegroundPredicate.h"
#include <DGtal/topology/SurfelAdjacency.h>
#include "DGtal/topology/LightImplicitDigitalSurface.h"
#include "DGtal/topology/DigitalSurface.h"
#include "DGtal/topology/CanonicDigitalSurfaceEmbedder.h"
#include "DGtal/geometry/surfaces/estimation/BasicConvolutionWeights.h"
#include "DGtal/geometry/surfaces/estimation/LocalConvolutionNormalVectorEstimator.h"
#include "DGtal/kernel/CanonicEmbedder.h"
#include "DGtal/geometry/surfaces/estimation/CNormalVectorEstimator.h"
#include "DGtal/geometry/surfaces/estimation/DigitalSurfaceEmbedderWithNormalVectorEstimator.h"
#include <DGtal/geometry/volumes/distance/DistanceTransformation.h>

using namespace std;
using namespace DGtal;
using namespace Z3i;

// Compute the profile
int ToolGeom::computeProfile(std::string inputFilename, bool normal, bool mean,  DGtal::Z3i::Point p){
  std::string extension = inputFilename.substr(inputFilename.find_last_of(".") + 1);
  if(extension.compare("nrrd") != 0){
    std::cout << "not .nrrd" << std::endl;
    return -1;
  }

  trace.beginBlock("Load file .nrrd.");
  typedef  DGtal::ImageContainerBySTLVector<Z3i::Domain, unsigned char> Image3D;
  Image3D image = ITKReader<Image3D>::importITK(inputFilename);
  trace.endBlock();

  if(!image.domain().isInside(p)){
    std::cout << "The point is not in the image domain!" << std::endl;
    return -1;
    }

  int x= p[0];
  int y= p[1];
  int z= p[2];

  // Compute the center
  int cx;
  int cy;
  int cz;
  cx = ((image.domain().upperBound())[0]+1)/2;
  cy = ((image.domain().upperBound())[1]+1)/2;
  cz = ((image.domain().upperBound())[2]+1)/2;

  double deltax = 0.0;
  double deltay = 0.0;
  double deltaz = 0.0;
  double dist = 0.0;
  int nbPas = 0;

  // Compute the slope
  if(normal){
    double normals[3];
    normals[0] = 0.0;
    normals[1] = 0.0;
    normals[2] = 0.0;
    computeNormals(image, normals, p);
    std::cout << "normals[0] = " << normals[0]  << std::endl;
    std::cout << "normals[1] = " << normals[1]  << std::endl;
    std::cout << "normals[2] = " << normals[2]  << std::endl;
    deltax = -1 * normals[0];
    deltay = -1 * normals[1];
    deltaz = -1 * normals[2];
    nbPas = 100;
  }else{
    deltax = cx-x;
    deltay = cy-y;
    deltaz = cz-z;
    dist = sqrt(deltax * deltax + deltay*deltay + deltaz*deltaz);
    deltax = deltax/dist;
    deltay = deltay/dist;
    deltaz = deltaz/dist;
    nbPas = (int)dist;
  }

  int nvPx = 0;
  int nvPy = 0;
  int nvPz = 0;
  int valPixel = 0;
  std::string n = "false";
  std::string m = "false";
  if(normal) n = "true";
  if(mean) m = "true";
  std::ofstream outP(inputFilename + "_N" + n + "_M" + m + "_profile.txt");
  for(int k=0; k<nbPas; k++){
      nvPx = x + k * deltax;
      nvPy = y + k * deltay;
      nvPz = z + k * deltaz;
      valPixel = image(DGtal::Z3i::Point(nvPx, nvPy, nvPz));
      outP << nvPx << " " << nvPy << " " << nvPz << " " << valPixel  << std::endl;
  }
  std::cout << std::endl;
  outP.close();
  return 0;
}

// Compute the normals
int ToolGeom::computeNormals(Image3D image, double *normals, DGtal::Z3i::Point p){
  clock_t start, end;
  start = clock();
  int level = 1;
  functors::SimpleThresholdForegroundPredicate<Image3D> simplePredicate(image,level);
  KSpace ks;
  bool space_ok = ks.init(image.domain().lowerBound(), image.domain().upperBound(), true);
  if(!space_ok){
    trace.error() << "Error in the Khalimsky space construction." << std::endl;
    return 2;
  }

  typedef SurfelAdjacency<KSpace::dimension> MySurfelAdjacency;
  MySurfelAdjacency surfAdj (true); // Interior in all directions.

  // Set up digital surface.
  typedef LightImplicitDigitalSurface<KSpace, functors::SimpleThresholdForegroundPredicate<Image3D>> MyDigitalSurfaceContainer;
  typedef DigitalSurface<MyDigitalSurfaceContainer> MyDigitalSurface;
  SCell bel = Surfaces<KSpace>::findABel(ks, simplePredicate);

  MyDigitalSurfaceContainer* ptrSurfContainer = new MyDigitalSurfaceContainer (ks, simplePredicate, surfAdj, bel);
  MyDigitalSurface digSurf (ptrSurfContainer); // Acquired
  MyDigitalSurface::ConstIterator it = digSurf.begin();

  // Embedder definition
  typedef CanonicDigitalSurfaceEmbedder<MyDigitalSurface> SurfaceEmbedder;
  SurfaceEmbedder surfaceEmbedder(digSurf);

  // Convolution kernel
  double sigma = 5.0;
  deprecated::GaussianConvolutionWeights<MyDigitalSurface::Size> Gkernel(sigma);

  // Estimator definition
  typedef deprecated::LocalConvolutionNormalVectorEstimator <MyDigitalSurface, deprecated::GaussianConvolutionWeights<MyDigitalSurface::Size>> MyGaussianEstimator;
  BOOST_CONCEPT_ASSERT ((concepts::CNormalVectorEstimator<MyGaussianEstimator>));
  MyGaussianEstimator myNormalEstimatorG(digSurf, Gkernel);

  // Embedder definition
  typedef DigitalSurfaceEmbedderWithNormalVectorEstimator<SurfaceEmbedder,MyGaussianEstimator> SurfaceEmbedderWithGaussianNormal;
  SurfaceEmbedderWithGaussianNormal mySurfelEmbedderG(surfaceEmbedder, myNormalEstimatorG);

  // Compute normal vector field and displays it.
  unsigned int neighborhood = 10;
  myNormalEstimatorG.init (1.0, neighborhood);

  MyGaussianEstimator::Quantity res;
  int cpt = 0;
  
  for(MyDigitalSurface::ConstIterator it = digSurf.begin(), itend = digSurf.end(); it != itend; ++it){
    res = myNormalEstimatorG.eval(it);
    std::cout << " res[0]= " << res[0];
    std::cout << " res[1]= " << res[1];
    std::cout << " res[2]= " << res[2];
    std::cout << std::endl;
    cpt++;
    if(cpt>20) break;
  }

  // Calculating total time taken by the program.
  end = clock();
  double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time computing : " << fixed << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

  normals[0] = (double) res[0];
  normals[1] = (double) res[1];
  normals[2] = (double) res[2];

  return 0;
}