#ifndef TOOLIMAGE3D_H
#define TOOLIMAGE3D_H

#include <string>
#include <QStringList>

// Define class ToolImage3D
class ToolImage3D{

public:
    int convert3D(int begin, int end, std::string inputDirectory, std::string outputDirectory, std::string outputFilename, std::string size, QStringList fileList);

protected:

private:

};
#endif