#ifndef TOOLIMAGE_H
#define TOOLIMAGE_H

#include <string>

// Define class ToolImage
class ToolImage{

public:
    int display(const std::string filename);
    int convert(std::string inputfilename, std::string outputfilename, std::string size);
    void segmentation_otsu();

protected:

private:

};
#endif
