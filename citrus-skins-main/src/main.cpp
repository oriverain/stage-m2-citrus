#include <iostream>
#include <QApplication>
#include "gui/MainWindow.h"

// Main class for the program
int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return app.exec();
}
