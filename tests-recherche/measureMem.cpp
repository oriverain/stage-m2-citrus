#include "itkMemoryProbe.h"
#include <iostream>
#include "itkImage.h"
#include "itkImageFileReader.h"

int
main(int argc, char * argv[])
{
  if (argc < 2)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile" << std::endl;
    return EXIT_FAILURE;
  }

  std::string inputFilename = argv[1];
  std::string extension = inputFilename.substr(inputFilename.find_last_of(".") + 1);
  
  using PixelType = unsigned int;
  using ImageType = itk::Image<PixelType, 3>;  
  if(extension.compare("nrrd") != 0){
    std::cout << "not .nrrd" << std::endl;
    using ImageType = itk::Image<PixelType, 2>;  
  }

  itk::MemoryProbe memoryProbe;

  memoryProbe.Start();
  std::cout << "We are measuring " << memoryProbe.GetType();
  //std::cout << " in units of " << memoryProbe.GetUnit() << ".\n" << std::endl;
  std::cout << " in units of MB"  << ".\n" << std::endl;
  memoryProbe.Stop();

  // Expect zeros here
  std::cout << "** Start **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << std::endl;

  memoryProbe.Start();
  ImageType::Pointer image = itk::ReadImage<ImageType>(argv[1]);

  ImageType::RegionType region = image->GetLargestPossibleRegion();

  ImageType::SizeType size = region.GetSize();

  std::cout << size << std::endl;
 
  memoryProbe.Stop();

  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;

  memoryProbe.Start();
  
  memoryProbe.Stop();

  std::cout << "** After deallocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  return EXIT_SUCCESS;
}