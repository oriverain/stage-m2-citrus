#include "itkTileImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkNumericSeriesFileNames.h"
#include "itkMemoryProbe.h"

#include <string>
#include <filesystem>
using namespace std;

int
main(int argc, char * argv[])
{

  using PixelType = unsigned int;
  
  constexpr unsigned int InputImageDimension = 2;
  constexpr unsigned int OutputImageDimension = 3;

  using InputImageType = itk::Image<PixelType, InputImageDimension>;
  using OutputImageType = itk::Image<PixelType, OutputImageDimension>;

  using ImageReaderType = itk::ImageFileReader<InputImageType>;

  using TilerType = itk::TileImageFilter<InputImageType, OutputImageType>;

  if (argc < 4)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << "inputDirectory begin end output" << std::endl;
    return EXIT_FAILURE;
  }

  clock_t start, cend;
  double time_taken;
  start = clock();
  itk::MemoryProbe memoryProbe;

  memoryProbe.Start();
  std::cout << "We are measuring " << memoryProbe.GetType();
  //std::cout << " in units of " << memoryProbe.GetUnit() << ".\n" << std::endl;
  std::cout << " in units of MB"  << ".\n" << std::endl;
  

  std::string inputDirectory = argv[argc-4];
  uint begin = stoi(argv[argc-3]);
  uint end = stoi(argv[argc-2]);

  std::vector<std::string> names ;  
  for (const auto & entry : std::filesystem::directory_iterator(inputDirectory))
    {  
      names.push_back(entry.path());      
    }
  
  std::sort(names.begin(), names.end()); // sort
  //std::vector<std::string>::iterator nit;  
  /*for (nit = names.begin(); nit != names.end(); ++nit)
  {
    std::cout << "File: " << (*nit).c_str() << std::endl;    
  }*/
  std::cout << "names size = " << names.size() << std::endl;
  std::cout << "names end = " << *(names.end()-1) << std::endl;
  
  std::string extension = (*(names.end()-1)).substr((*(names.end()-1)).find_last_of(".") + 1);
  std::cout << "extension = " << extension << std::endl;
  if(extension.compare("info") ==0) names.pop_back();
  std::cout << "names size = " << names.size() << std::endl;
  std::cout << "names end = " << *(names.end()-1) << std::endl;
  
  for (int i=begin; i<end+1; i++)
  {
    std::cout << "File: " << names.at(i) << std::endl;    
  }
 
  auto tiler = TilerType::New();

  itk::FixedArray<unsigned int, OutputImageDimension> layout;

  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 0;

  tiler->SetLayout(layout);

  unsigned int inputImageNumber = 0;

  auto reader = ImageReaderType::New();

  InputImageType::Pointer inputImageTile;

  for (int i=begin; i<end+1; i++)
  {
    reader->SetFileName(names.at(i));
    reader->UpdateLargestPossibleRegion();
    inputImageTile = reader->GetOutput();
    inputImageTile->DisconnectPipeline();
    tiler->SetInput(inputImageNumber++, inputImageTile);
  }

  std::cout << "inputImageNumber = " << inputImageNumber << std::endl; 

  PixelType filler = 128;

  tiler->SetDefaultPixelValue(filler);

  tiler->Update();
  cend = clock();
  time_taken = double(cend - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time prepare tiles : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

  start = clock();
  try
  {
    itk::WriteImage(tiler->GetOutput(), argv[argc - 1]);
  }
  catch (const itk::ExceptionObject & excp)
  {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
  }

  cend = clock();
  time_taken = double(cend - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time write : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;
  memoryProbe.Stop();

  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;

  return EXIT_SUCCESS;
}