#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkImage.h"
#include <itkJPEG2000ImageIO.h>

int
main(int argc, char ** argv)
{
  // Verify the number of parameters in the command line
  if (argc < 7)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile  outputImageFile " << std::endl;
    std::cerr << " startX startY startZ sizeX sizeY sizeZ" << std::endl;
    return EXIT_FAILURE;
  }
 
  using InputPixelType = unsigned int;
  using OutputPixelType = unsigned int;
  //using InputPixelType = unsigned char;
  //using OutputPixelType = unsigned char;
  constexpr unsigned int Dimension = 3; 
  using InputImageType = itk::Image<InputPixelType, Dimension>;  
  using OutputImageType = itk::Image<OutputPixelType, Dimension>;  

  using ReaderType = itk::ImageFileReader<InputImageType>;
  using WriterType = itk::ImageFileWriter<OutputImageType>;

  using FilterType = itk::RegionOfInterestImageFilter<InputImageType, OutputImageType>; 
  auto filter = FilterType::New();
  
  OutputImageType::IndexType start;
  start[0] = std::stoi(argv[3]);
  start[1] = std::stoi(argv[4]);
  start[2] = std::stoi(argv[5]);;
  OutputImageType::SizeType size;
  size[0] = std::stoi(argv[6]);
  size[1] = std::stoi(argv[7]);
  size[2] = std::stoi(argv[8]);;
  
  OutputImageType::RegionType desiredRegion;
  desiredRegion.SetSize(size);
  desiredRegion.SetIndex(start);
  filter->SetRegionOfInterest(desiredRegion);
  
  auto reader = ReaderType::New();
  auto writer = WriterType::New();
  const char * inputFilename = argv[1];
  const char * outputFilename = argv[2];  
  reader->SetFileName(inputFilename);
  writer->SetFileName(outputFilename);

  filter->SetInput(reader->GetOutput());
  writer->SetInput(filter->GetOutput());

    std::cout << "ici 1" << std::endl;

    try
  {
    writer->Update();
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }
  // Software Guide : EndCodeSnippet
 
  std::cout << "ici 2" << std::endl;
  return EXIT_SUCCESS;
}
