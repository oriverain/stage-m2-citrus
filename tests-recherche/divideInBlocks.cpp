#include <string>
#include <filesystem>
#include <vector>
using namespace std;

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"
#include "itkTileImageFilter.h"
#include "itkImageFileWriter.h"

int
main(int argc, char * argv[])
{

  // Verify command line arguments
  if (argc < 3)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputdirectory" << "outputDirectory" <<std::endl;
    return EXIT_FAILURE;
  }
  std::string inputDirectory = argv[1];
  std::string outputDirectory = argv[2];
  clock_t cstart, cend;
  double time_taken;
  itk::MemoryProbe memoryProbe;

  std::cout << "We are measuring " << memoryProbe.GetType();
  std::cout << " in units of MB"  << ".\n" << std::endl;

  using PixelType = unsigned int;
  using ImageType2D = itk::Image<PixelType, 2>;
  using ImageType3D = itk::Image<PixelType, 3>;
  using TilerType = itk::TileImageFilter<ImageType2D, ImageType3D>;

  std::vector<std::string> names ;  
  for (const auto & entry : std::filesystem::directory_iterator(inputDirectory))
    {  
      names.push_back(entry.path());        
    }
  
  std::sort(names.begin(), names.end()); // sort
    
  std::string extension = (*(names.end()-1)).substr((*(names.end()-1)).find_last_of(".") + 1);  
  if(extension.compare("info") ==0) names.pop_back();
  std::cout << "names size = " << names.size() << std::endl;
  

  cstart = clock();
  memoryProbe.Start();

 // compute the number of blocks and the size of one block
  ImageType2D::Pointer image0 = itk::ReadImage<ImageType2D>(names.at(0));
  ImageType2D::RegionType region0 = image0->GetLargestPossibleRegion();
  ImageType2D::SizeType size0 = region0.GetSize();
  std::cout << "size 0 = " << size0 << std::endl; 
  std::cout << "first file: " << names.at(0) << std::endl;

  double fileSizeInBytes = region0.GetNumberOfPixels() * sizeof(PixelType);
  double fileSizeInMB = fileSizeInBytes / (1024.0 * 1024.0);

  std::cout << "Taille du fichier : " << fileSizeInMB << " Mo" << std::endl;
 


  memoryProbe.Stop();
  cend = clock(); 
  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;
  time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
  std::cout << "Time  : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

 return EXIT_SUCCESS;
}