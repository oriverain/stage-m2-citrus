#include <string>
#include <filesystem>
#include <chrono>

using namespace std;

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"
#include "itkImageFileWriter.h"

#include "tools/ToolsItk.h"

int
main(int argc, char * argv[])
{

    // Verify command line arguments
    if (argc < 12)
    {
        std::cerr << "Usage: " << std::endl;
        std::cerr << argv[0] << " inputdirectory" << " sizex sizey sizez px py pz posInArea outputFile factorResize extension nbTests"<< " / positionInArea = c for center or o for origin" <<std::endl;
        return EXIT_FAILURE;
    }

    std::string inputDirectory = argv[1];
    uint sizeX = stoi(argv[2]);
    uint sizeY = stoi(argv[3]);
    uint sizeZ = stoi(argv[4]);
    uint px = stoi(argv[5]);
    uint py = stoi(argv[6]);
    uint pz = stoi(argv[7]);
    std::string positionInArea = argv[8];
    std::string outputFilename = argv[9];
    uint factorResize = stoi(argv[10]);
    std::string extension = argv[11];
    int nbTests = atoi(argv[12]); 
  
    itk::MemoryProbe memoryProbe;

    std::cout << "We are measuring " << memoryProbe.GetType();
    std::cout << " in units of MB"  << ".\n" << std::endl;  
  
    std::chrono::duration<double> parallel_duration; 

    ToolsItk tool ;
    int res =0;
    double tabTime[nbTests];
    double totalTime =0;
    double tabMem[nbTests];
    double totalMemory =0;
    for(uint i=0; i< nbTests; i++) {
        std::cout << "** Test " << i << " **" << std::endl;
        memoryProbe.Start();
        auto start_timeP = std::chrono::high_resolution_clock::now();
        int res = tool.createRoi(inputDirectory, sizeX, sizeY, sizeZ, px, py, pz, positionInArea, outputFilename, factorResize, extension);
        std::cout << "res :" << res << std::endl;
        auto end_timeP = std::chrono::high_resolution_clock::now();
        parallel_duration  = end_timeP - start_timeP;
        memoryProbe.Stop();
        tabTime[i] = parallel_duration.count();
        totalTime += tabTime[i];
        tabMem[i] = memoryProbe.GetTotal()/1000;
        totalMemory += tabMem[i];        
    }

    std::cout << "--------------------------------" << std::endl;
    std::cout << "** Parallel duration (seconds) **" << std::endl;
    for(uint i=0; i< nbTests; i++) {
        std::cout << tabTime[i] << " " ;
    }
    std::cout << std::endl;

    std::cout << "mean time = " << totalTime/nbTests << std::endl;

    std::cout << "** Memory **" << std::endl;
    for(uint i=0; i< nbTests; i++) {
        std::cout << tabMem[i] << " " ;
    }
    std::cout << std::endl;
    std::cout << "mean memory = " << totalMemory/nbTests << std::endl;
        

    return EXIT_SUCCESS;
}