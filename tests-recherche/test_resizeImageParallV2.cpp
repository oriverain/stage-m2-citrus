#include <string>
#include <filesystem>
#include <chrono>

using namespace std;

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"
#include "itkImageFileWriter.h"

#include "tools/ToolsItk.h"

int
main(int argc, char * argv[])
{

    // Verify command line arguments
    if (argc < 8)
    {
        std::cerr << "Usage: " << std::endl;
        std::cerr << argv[0] << " inputDirectory begin end factorResize outputImage extension nbTests" << "  example factorResize = 8 to divide the size by 8" <<std::endl;
        return EXIT_FAILURE;
    }

    std::string inputDirectory = argv[1];
    int begin = atoi(argv[2]);
    int end = atoi(argv[3]);  
    int factorResize = atoi(argv[4]);
    std::string outputImage = argv[5];
    std::string extension = argv[6];
    int nbTests = atoi(argv[7]);  

    std::cout << "inputDirectory = " << inputDirectory << std::endl;
    std::cout << "begin = " << begin << std::endl;
    std::cout << "end = " << end << std::endl;
    std::cout << "factorResize = " << factorResize << std::endl;
    std::cout << "outputImage = " << outputImage << std::endl;
    std::cout << "nbTests = " << nbTests << std::endl;
  
    itk::MemoryProbe memoryProbe;

    std::cout << "We are measuring " << memoryProbe.GetType();
    std::cout << " in units of MB"  << ".\n" << std::endl;  
  
    std::chrono::duration<double> parallel_duration; 

    ToolsItk tool ;
    int res =0;
    double tabTime[nbTests];
    double totalTime =0;
    double tabMem[nbTests];
    double totalMemory =0;
    for(uint i=0; i< nbTests; i++) {
        std::cout << "** Test " << i << " **" << std::endl;
        memoryProbe.Start();
        auto start_timeP = std::chrono::high_resolution_clock::now();
        res = tool.resizeImageParallV2(inputDirectory, begin, end, factorResize, outputImage, extension);
        std::cout << "res :" << res << std::endl;
        auto end_timeP = std::chrono::high_resolution_clock::now();
        parallel_duration  = end_timeP - start_timeP;
        memoryProbe.Stop();
        tabTime[i] = parallel_duration.count();
        totalTime += tabTime[i];
        tabMem[i] = memoryProbe.GetTotal()/1000;
        totalMemory += tabMem[i];        
    }

    std::cout << "--------------------------------" << std::endl;
    std::cout << "** Parallel duration (seconds) **" << std::endl;
    for(uint i=0; i< nbTests; i++) {
        std::cout << tabTime[i] << " " ;
    }
    std::cout << std::endl;

    std::cout << "mean time = " << totalTime/nbTests << std::endl;

    std::cout << "** Memory **" << std::endl;
    for(uint i=0; i< nbTests; i++) {
        std::cout << tabMem[i] << " " ;
    }
    std::cout << std::endl;
    std::cout << "mean memory = " << totalMemory/nbTests << std::endl;
        

    return EXIT_SUCCESS;
}