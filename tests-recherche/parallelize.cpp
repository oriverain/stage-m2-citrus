#include <string>
#include <filesystem>
#include <omp.h>
#include <chrono>
#include <iostream>

using namespace std;

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"
#include "itkImageFileWriter.h"

#include "tools/ToolsItk.h"

int
main(int argc, char * argv[])
{

  // Verify command line arguments
    if (argc < 4)
    {
        std::cerr << "Usage: " << std::endl;
        std::cerr << argv[0] << " inputDirectory begin end " << std::endl;
        return EXIT_FAILURE;
    }

    std::string inputDirectory = argv[1];
    int begin = atoi(argv[2]);
    int end = atoi(argv[3]);  
  

    std::cout << "inputDirectory = " << inputDirectory << std::endl;
    std::cout << "begin = " << begin << std::endl;
    std::cout << "end = " << end << std::endl;
  
  

    clock_t cstart, cend;
    double time_taken;
    itk::MemoryProbe memoryProbe;
    std::vector<std::string> names ;
    using PixelType = unsigned int;
    using ImageType = itk::Image<PixelType, 2>;

    for (const auto & entry : std::filesystem::directory_iterator(inputDirectory))
        {  
        names.push_back(entry.path());      
        }
  
    std::sort(names.begin(), names.end()); // sort
  
    std::cout << "names size = " << names.size() << std::endl;
    std::cout << "names end = " << *(names.end()-1) << std::endl;
  
    std::string extension = (*(names.end()-1)).substr((*(names.end()-1)).find_last_of(".") + 1);
    std::cout << "extension = " << extension << std::endl;
    if(extension.compare("info") ==0) names.pop_back();
    std::cout << "names size = " << names.size() << std::endl;
    std::cout << "names end = " << *(names.end()-1) << std::endl;

    std::cout << "We are measuring " << memoryProbe.GetType();
    std::cout << " in units of MB"  << ".\n" << std::endl;
    

  //ToolsItk tool ;
 // int res = tool.resizeImage(inputDirectory, begin, end, factorResize, outputImage);
  //std::cout << "res :" << res << std::endl;
    
    // serial
    cstart = clock();
    memoryProbe.Start();
    auto start_timeS = std::chrono::high_resolution_clock::now(); 
    for(uint i=begin; i<end+1; i++) {
        //std::cout << "file = " << names.at(i) << std::endl;
        ImageType::Pointer image = itk::ReadImage<ImageType>(names.at(i));
        ImageType::RegionType region = image->GetLargestPossibleRegion();   
    }
    auto end_timeS = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> serial_duration  = end_timeS - start_timeS;
    std::cout << "Serial duration: "
              << serial_duration.count() << " seconds"
              << std::endl; 
    memoryProbe.Stop();
    cend = clock(); 
    std::cout << "** Serial After allocation **" << std::endl;
    std::cout << "Mean: " << memoryProbe.GetMean()/1000 << std::endl;
    std::cout << "Total: " << memoryProbe.GetTotal()/1000 << std::endl;
    std::cout << "Max: " << memoryProbe.GetMaximum()/1000 << std::endl;
    std::cout << std::endl;
    time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
    std::cout << "Serial Time  : " << fixed
         << time_taken << setprecision(5);
    std::cout << " sec " << std::endl;
    std::cout << std::endl;
    
    cstart = clock();
    memoryProbe.Start();
    auto start_timeP = std::chrono::high_resolution_clock::now(); 
    #pragma omp parallel for
    for(uint i=begin; i<end+1; i++) {
        ImageType::Pointer image = itk::ReadImage<ImageType>(names.at(i));
        ImageType::RegionType region = image->GetLargestPossibleRegion();   
    }

    auto end_timeP = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> parallel_duration  = end_timeP - start_timeP;
    std::cout << "Parallel duration: "
              << parallel_duration.count() << " seconds"
              << std::endl; 

    memoryProbe.Stop();
    cend = clock(); 
    std::cout << "** Parallele After allocation **" << std::endl;
    std::cout << "Mean: " << memoryProbe.GetMean()/1000 << std::endl;
    std::cout << "Total: " << memoryProbe.GetTotal()/1000 << std::endl;
    std::cout << "Max: " << memoryProbe.GetMaximum()/1000 << std::endl;
    std::cout << std::endl;
    time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
    std::cout << "Parallele Time  : " << fixed
         << time_taken << setprecision(5);
    std::cout << " sec " << std::endl;


    std::cout << "facteur de performance = " << serial_duration.count()/parallel_duration.count() << std::endl;

    return EXIT_SUCCESS;
}