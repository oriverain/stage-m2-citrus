#include "itkPipelineMonitorImageFilter.h"
#include "itkStreamingImageFilter.h"
#include "itkMemoryProbe.h"
#include "itkImage.h"
#include "itkImageFileReader.h"

using namespace std;
 
int
main(int argc, char * argv[])
{
  if (argc < 3)
  {
    std::cerr << "Usage: " << argv[0] << " inputFileName <NumberOfSplits>" << std::endl;
    return EXIT_FAILURE;
  }
  int numberOfSplits = std::stoi(argv[2]);
  std::string inputFileName = argv[1];

  clock_t cstart, cend;
  double time_taken;
  itk::MemoryProbe memoryProbe;
  
  std::cout << "We are measuring " << memoryProbe.GetType();
  //std::cout << " in units of " << memoryProbe.GetUnit() << ".\n" << std::endl;
  std::cout << " in units of MB"  << ".\n" << std::endl;
 
  constexpr unsigned int Dimension = 3;
  using PixelType = unsigned int;
  using ImageType = itk::Image<PixelType, Dimension>;

  cstart = clock();
  memoryProbe.Start();
 
  //using SourceType = itk::RandomImageSource<ImageType>;
  //auto source = SourceType::New();
  ImageType::Pointer image = itk::ReadImage<ImageType>(inputFileName);
  //ImageType::SizeType size;
  //size.Fill(numberOfSplits);
  //source->SetSize(size);  
  //std::cout << "source size = " << *(source->GetSize()) << std::endl;
  ImageType::RegionType region = image->GetLargestPossibleRegion();
  ImageType::SizeType size = region.GetSize();

  std::cout << "Dimensions de l'image : ";
  for (unsigned int i = 0; i < Dimension; ++i)
  {
    std::cout << size[i] << " ";
  }
  std::cout << std::endl;

  double fileSizeInBytes = region.GetNumberOfPixels() * sizeof(PixelType);
  double fileSizeInMB = fileSizeInBytes / (1024.0 * 1024.0);

  std::cout << "Taille du fichier : " << fileSizeInMB << " Mo" << std::endl;
 
  using MonitorFilterType = itk::PipelineMonitorImageFilter<ImageType>;
  auto monitorFilter = MonitorFilterType::New();
  //monitorFilter->SetInput(source->GetOutput());
  // If ITK was built with the Debug CMake configuration, the filter
  // automatically outputs status information to the console
  //monitorFilter->DebugOn();
  monitorFilter->SetInput(image);
 
  using StreamingFilterType = itk::StreamingImageFilter<ImageType, ImageType>;
  auto streamingFilter = StreamingFilterType::New();
  streamingFilter->SetInput(monitorFilter->GetOutput());
  //streamingFilter->SetInput(image);
  streamingFilter->SetNumberOfStreamDivisions(numberOfSplits);
 
  try
  {
    streamingFilter->Update();
  }
  catch (const itk::ExceptionObject & error)
  {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
  }
 
  memoryProbe.Stop();
  cend = clock(); 
  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;
  time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
  std::cout << "Time  : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;


  std::cout << "The output LargestPossibleRegion is: " << streamingFilter->GetOutput()->GetLargestPossibleRegion()
            << std::endl;
  std::cout << std::endl;
 
  const MonitorFilterType::RegionVectorType updatedRequestedRegions = monitorFilter->GetUpdatedRequestedRegions();
  std::cout << "Updated RequestedRegions's:" << std::endl;
  for (const auto & updatedRequestedRegion : updatedRequestedRegions)
  {
    std::cout << "  " << updatedRequestedRegion << std::endl;
  }
 
  ImageType::IndexType pixelIndex;
  pixelIndex[0] = 10; 
  pixelIndex[1] = 20; 
  pixelIndex[2] = 30;
  ImageType::PixelType pixelValue = streamingFilter->GetOutput()->GetPixel(pixelIndex);

  std::cout << "La valeur du pixel à la position (10, 20, 30) est : " << static_cast<int>(pixelValue) << std::endl;

  pixelIndex[0] = 1000; 
  pixelIndex[1] = 1220; 
  pixelIndex[2] = 490;
  pixelValue = streamingFilter->GetOutput()->GetPixel(pixelIndex);

  std::cout << "La valeur du pixel à la position (1000, 1220, 490) est : " << static_cast<int>(pixelValue) << std::endl;


  return EXIT_SUCCESS;
}
