#include "itkImage.h"
#include "itkImageFileWriter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkImageFileReader.h"



//using ImageType = itk::Image<unsigned char, 2>;
//using ImageType = itk::Image<unsigned int, 2>;
using ImageType = itk::Image<unsigned int, 3>;

int
main(int argc, char * argv[])
{
  if (argc < 2)
  {
    std::cerr << "Required: filename" << std::endl;

    return EXIT_FAILURE;
  }

  const auto input = itk::ReadImage<ImageType>(argv[1]);

  using ImageCalculatorFilterType = itk::MinimumMaximumImageCalculator<ImageType>;

  auto imageCalculatorFilter = ImageCalculatorFilterType::New();
  imageCalculatorFilter->SetImage(input);
  imageCalculatorFilter->Compute();

  

  
  ImageType::IndexType   minimumLocation = imageCalculatorFilter->GetIndexOfMinimum();
  std::cout << "minimumLocation[0] = " << minimumLocation[0] << std::endl;
  std::cout << "minimumLocation[1] = " << minimumLocation[1] << std::endl;
  std::cout << "minimumLocation[2] = " << minimumLocation[2] << std::endl; 
  std::cout << "value min = " << (uint)input->GetPixel(minimumLocation) << std::endl;
  
  ImageType::IndexType maximumLocation = imageCalculatorFilter->GetIndexOfMaximum();
  std::cout << "maximumLocation[0] = " << maximumLocation[0] << std::endl;
  std::cout << "maximumLocation[1] = " << maximumLocation[1] << std::endl;
  std::cout << "maximumLocation[2] = " << maximumLocation[2] << std::endl;
  std::cout << "value max = " << (uint)input->GetPixel(maximumLocation) << std::endl;

  

  ImageType::RegionType region = input->GetLargestPossibleRegion();

  ImageType::SizeType size = region.GetSize();

  std::cout << size << std::endl;
  

  return EXIT_SUCCESS;
}