#include <string>
#include <filesystem>
#include <vector>
using namespace std;

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"
#include "itkImageFileWriter.h"
#include "itkMaximumProjectionImageFilter.h"
#include "itkExtractImageFilter.h"

#include "tools/ToolsItk.h"

int
main(int argc, char * argv[])
{

  // Verify command line arguments
  if (argc < 3)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " axe begin end inputImage outputImage" << "  où axe =x, y,  z pour la direction de la projection" <<std::endl;
    return EXIT_FAILURE;
  }

  std::string axe = argv[1];
  int begin = stoi(argv[2]);
  int end = stoi(argv[3]);
  std::string inputImage = argv[4];
  std::string outputImage = argv[5];

  clock_t cstart, cend;
  double time_taken;
  itk::MemoryProbe memoryProbe;

  std::cout << "We are measuring " << memoryProbe.GetType();
  std::cout << " in units of MB"  << ".\n" << std::endl;
  cstart = clock();
  memoryProbe.Start();

  ToolsItk tool;

  //int res = tool.mip(inputDirectory, outputImage, axe, param);  // mip(std::string inputDirectory, ImageType2D::Pointer imageMip, char axe, int param) 
  using ImageType2D = itk::Image<PixelType, 2>;
  using ImageType3D = itk::Image<PixelType, 3>;  

  int res = tool.mip3(inputImage, outputImage, axe, begin, end);
  
  
  /*using PixelType = unsigned int;
  using ImageType2D = itk::Image<PixelType, 2>;
  using ImageType3D = itk::Image<PixelType, 3>;

  using ImageReaderType = itk::ImageFileReader<ImageType3D>;
  ImageReaderType::Pointer reader = ImageReaderType::New();
  reader->SetFileName(argv[3]);
  using FilterType = itk::MaximumProjectionImageFilter< ImageType3D, ImageType3D >;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput(reader->GetOutput());
  filter->Update();
  ImageType3D::SizeType inputSize = filter->GetOutput()->GetLargestPossibleRegion().GetSize();
  std::cout << "inputSize = " << inputSize << std::endl;
  using WriterType = itk::ImageFileWriter<ImageType3D>;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName(argv[4]);
  writer->SetInput(filter->GetOutput());

  try {
    writer->Update();
  } catch (itk::ExceptionObject &error) {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
  }*/


/*using ExtractType = itk::ExtractImageFilter< ImageType3D, ImageType2D >;
ExtractType::Pointer extract = ExtractType::New();
extract->SetDirectionCollapseToIdentity() ;
extract->SetInput(filter->GetOutput());
ImageType3D::SizeType size;
for(int i=0; i<3; i++) {
    if(i == axe) {
        size[i] = 0;
    } else {
    size[i] = inputSize[i];
    }
}
ImageType3D::IndexType idx;
idx.Fill(0);
ImageType3D::RegionType region;
region.SetSize(size);
region.SetIndex(idx);
extract->SetExtractionRegion(region);
//typedef itk::ImageFileWriter< IType2 > WriterType;
using WriterType = itk::ImageFileWriter< ImageType2D >;
WriterType::Pointer writer = WriterType::New();
writer->SetInput(extract->GetOutput());
writer->SetFileName(argv[3]);
writer->Update();*/


  memoryProbe.Stop();
  cend = clock(); 
  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;
  time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
  std::cout << "Time  : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

 return EXIT_SUCCESS;
}