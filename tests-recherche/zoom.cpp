#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkImage.h"
#include <itkJPEG2000ImageIO.h>
#include "itkTIFFImageIO.h"

#include <string>

int
main(int argc, char ** argv)
{
  // Verify the number of parameters in the command line
  if (argc < 7)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile  outputImageFile " << std::endl;
    std::cerr << " startX startY sizeX sizeY" << std::endl;
    return EXIT_FAILURE;
  }
 
  std::string extensionI = ((std::string)argv[1]).substr(((std::string)argv[1]).find_last_of(".") + 1);
  std::cout << "extension inputFile= " << extensionI << std::endl;
  std::string extensionO = ((std::string)argv[2]).substr(((std::string)argv[2]).find_last_of(".") + 1);
  std::cout << "extension outputFile= " << extensionO << std::endl;
  //using InputPixelType = unsigned char;
  //using OutputPixelType = unsigned char;
  //if(extensionI.compare("jp2") == 0) using InputPixelType = unsigned int; //else using InputPixelType = unsigned char;
  //if(extensionO.compare("jp2") == 0) using OutputPixelType = unsigned int; //else using OutputPixelType = unsigned char;
  using InputPixelType = unsigned int;
  using OutputPixelType = unsigned int;
  constexpr unsigned int Dimension = 2; 
  using InputImageType = itk::Image<InputPixelType, Dimension>;  
  using OutputImageType = itk::Image<OutputPixelType, Dimension>;
  

  auto jp2IO = itk::JPEG2000ImageIO::New();
  auto tiffIO = itk::TIFFImageIO::New();
  
  using ReaderType = itk::ImageFileReader<InputImageType>;
  using WriterType = itk::ImageFileWriter<OutputImageType>;

  using FilterType = itk::RegionOfInterestImageFilter<InputImageType, OutputImageType>; 
  auto filter = FilterType::New();
  
  OutputImageType::IndexType start;
  start[0] = std::stoi(argv[3]);
  start[1] = std::stoi(argv[4]);  
  OutputImageType::SizeType size;
  size[0] = std::stoi(argv[5]);
  size[1] = std::stoi(argv[6]);  
  
  OutputImageType::RegionType desiredRegion;
  desiredRegion.SetSize(size);
  desiredRegion.SetIndex(start);
  filter->SetRegionOfInterest(desiredRegion);
  
  auto reader = ReaderType::New();
  auto writer = WriterType::New();
  const char * inputFilename = argv[1];
  const char * outputFilename = argv[2];
  if(extensionI.compare("jp2") == 0) reader->SetImageIO(jp2IO);
  if(extensionI.compare("tiff") == 0) reader->SetImageIO(tiffIO);
  reader->SetFileName(inputFilename);
  if(extensionO.compare("jp2") == 0) writer->SetImageIO(jp2IO);
  if(extensionO.compare("tiff") == 0) writer->SetImageIO(tiffIO);
  writer->SetFileName(outputFilename);

  filter->SetInput(reader->GetOutput());
  writer->SetInput(filter->GetOutput());

    std::cout << "ici 1" << std::endl;

    try
  {
    writer->Update();
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }
  // Software Guide : EndCodeSnippet
 
  std::cout << "ici 2" << std::endl;
  return EXIT_SUCCESS;
}
