#include "itkTileImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkNumericSeriesFileNames.h"
#include "itkMemoryProbe.h"
#include "itkStreamingImageFilter.h"
#include "itkPipelineMonitorImageFilter.h"

#include <string>
#include <filesystem>
using namespace std;

int
main(int argc, char * argv[])
{

  using PixelType = unsigned int;
  
  constexpr unsigned int InputImageDimension = 2;
  constexpr unsigned int OutputImageDimension = 3;

  using InputImageType = itk::Image<PixelType, InputImageDimension>;
  using OutputImageType = itk::Image<PixelType, OutputImageDimension>;

  //using ImageReaderType = itk::ImageFileReader<InputImageType>;
  using ImageReaderType = itk::ImageFileReader<OutputImageType>;

  using TilerType = itk::TileImageFilter<InputImageType, OutputImageType>;

  using StreamingFilterType = itk::StreamingImageFilter<OutputImageType, OutputImageType>;
  auto streamingFilter = StreamingFilterType::New();

  using MonitorFilterType = itk::PipelineMonitorImageFilter<OutputImageType>;
  auto monitorFilter = MonitorFilterType::New();
  //monitorFilter->DebugOn();

  if (argc < 5)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputDirectory begin end output NumberOfSplits" << std::endl;
    return EXIT_FAILURE;
  }

  clock_t start, cend;
  double time_taken;
  start = clock();
  itk::MemoryProbe memoryProbe;

  memoryProbe.Start();
  std::cout << "We are measuring " << memoryProbe.GetType();
  //std::cout << " in units of " << memoryProbe.GetUnit() << ".\n" << std::endl;
  std::cout << " in units of MB"  << ".\n" << std::endl;
  
  std::string inputDirectory = argv[1];  
  uint begin = stoi(argv[2]);
  uint end = stoi(argv[3]);
  std::string outputFilename = argv[4];  
  int numberOfSplits = std::stoi(argv[5]);
  

  std::vector<std::string> names ;  
  for (const auto & entry : std::filesystem::directory_iterator(inputDirectory))
    {  
      names.push_back(entry.path());      
    }
  
  std::sort(names.begin(), names.end()); // sort
  //std::vector<std::string>::iterator nit;  
  /*for (nit = names.begin(); nit != names.end(); ++nit)
  {
    std::cout << "File: " << (*nit).c_str() << std::endl;    
  }*/
  std::cout << "names size = " << names.size() << std::endl;
  std::cout << "names end = " << *(names.end()-1) << std::endl;
  
  std::string extension = (*(names.end()-1)).substr((*(names.end()-1)).find_last_of(".") + 1);
  std::cout << "extension = " << extension << std::endl;
  if(extension.compare("info") ==0) names.pop_back();
  std::cout << "names size = " << names.size() << std::endl;
  std::cout << "names end = " << *(names.end()-1) << std::endl;
  
  for (int i=begin; i<end+1; i++)
  {
    std::cout << "File: " << names.at(i) << std::endl;    
  }
 
  auto tiler = TilerType::New();

  itk::FixedArray<unsigned int, OutputImageDimension> layout;

  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 0;

  tiler->SetLayout(layout);

  unsigned int inputImageNumber = 0;

  auto reader = ImageReaderType::New();

  //InputImageType::Pointer inputImageTile;
  OutputImageType::Pointer inputImageTile;

  for (int i=begin; i<end+1; i++)
  {
    reader->SetFileName(names.at(i));
    reader->UpdateLargestPossibleRegion();
    inputImageTile = reader->GetOutput();
    monitorFilter->SetInput(inputImageTile);
    inputImageTile->DisconnectPipeline();
    //tiler->SetInput(inputImageNumber++, inputImageTile);
    inputImageTile->UpdateOutputInformation(); 
    //streamingFilter->SetInput(inputImageNumber++, inputImageTile);
    streamingFilter->SetInput(inputImageNumber++, monitorFilter->GetOutput());
    
  }

  std::cout << "inputImageNumber = " << inputImageNumber << std::endl; 

  PixelType filler = 128;

  //tiler->SetDefaultPixelValue(filler);

  //tiler->Update();
  streamingFilter->SetNumberOfStreamDivisions(numberOfSplits);
 
  try
  {
    streamingFilter->Update();
  }
  catch (const itk::ExceptionObject & error)
  {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
  }

std::cout << "The output LargestPossibleRegion is: " << streamingFilter->GetOutput()->GetLargestPossibleRegion()
            << std::endl;
  std::cout << std::endl;

  const MonitorFilterType::RegionVectorType updatedRequestedRegions = monitorFilter->GetUpdatedRequestedRegions();
  std::cout << "Updated RequestedRegions's:" << std::endl;
  for (const auto & updatedRequestedRegion : updatedRequestedRegions)
  {
    std::cout << "  " << updatedRequestedRegion << std::endl;
  }
 

  /*OutputImageType::IndexType pixelIndex;
  pixelIndex[0] = 10; 
  pixelIndex[1] = 20; 
  pixelIndex[2] = 9;
  OutputImageType::PixelType pixelValue = streamingFilter->GetOutput()->GetPixel(pixelIndex);
  std::cout << "La valeur du pixel à la position (10, 20, 9) est : " << static_cast<int>(pixelValue) << std::endl;

  pixelIndex[0] = 1000; 
  pixelIndex[1] = 1220; 
  pixelIndex[2] = 8;
  pixelValue = streamingFilter->GetOutput()->GetPixel(pixelIndex);

  std::cout << "La valeur du pixel à la position (1000, 1220, 8) est : " << static_cast<int>(pixelValue) << std::endl;
*/
  cend = clock();
  time_taken = double(cend - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time prepare tiles : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

  start = clock();
  using WriterType = itk::ImageFileWriter<OutputImageType>;
    WriterType::Pointer writer = WriterType::New();
    writer->SetFileName(outputFilename);
    writer->SetInput(streamingFilter->GetOutput());
  try
  {
    //itk::WriteImage(tiler->GetOutput(), outputFilename);
    //itk::WriteImage(streamingFilter->GetOutput(), outputFilename);
     writer->Update();
  }
  catch (const itk::ExceptionObject & excp)
  {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "outputFilename : " << outputFilename << std::endl;
  cend = clock();
  time_taken = double(cend - start) / double(CLOCKS_PER_SEC);
  std::cout << "Time write : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;
  memoryProbe.Stop();

  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;

  return EXIT_SUCCESS;
}