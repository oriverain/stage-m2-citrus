#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkMemoryProbe.h"

using namespace std;

int
main(int argc, char * argv[])
{
  // Verify command line arguments
  if (argc < 2)
  {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile" << std::endl;
    return EXIT_FAILURE;
  }

  clock_t cstart, cend;
  double time_taken;
  itk::MemoryProbe memoryProbe;

  std::cout << "We are measuring " << memoryProbe.GetType();
  std::cout << " in units of MB"  << ".\n" << std::endl;
  
  memoryProbe.Start();

  using PixelType = unsigned int;
  //using PixelType = int;

  //using ImageType = itk::Image<PixelType, 2>;
  using ImageType = itk::Image<PixelType, 3>;

  cstart = clock();
  ImageType::Pointer image = itk::ReadImage<ImageType>(argv[1]);

  ImageType::RegionType region = image->GetLargestPossibleRegion();

  cend = clock(); 

  ImageType::SizeType size = region.GetSize();

  std::cout << size << std::endl;

  memoryProbe.Stop();  
  std::cout << "** After allocation **" << std::endl;
  std::cout << "Mean: " << memoryProbe.GetMean()/1012 << std::endl;
  std::cout << "Total: " << memoryProbe.GetTotal()/1012 << std::endl;
  std::cout << "Max: " << memoryProbe.GetMaximum()/1012 << std::endl;
  std::cout << std::endl;
  time_taken = double(cend - cstart) / double(CLOCKS_PER_SEC);
  std::cout << "Time  : " << fixed
         << time_taken << setprecision(5);
  std::cout << " sec " << std::endl;

  
  return EXIT_SUCCESS;
}